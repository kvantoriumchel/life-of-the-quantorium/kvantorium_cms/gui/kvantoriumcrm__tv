import './App.sass';
import './grid.css'

import io from 'socket.io-client';

import Timetable from "./components/timetable/Timetable";
import PhotoGallery from "./components/photogallery/PhotoGallery";
import Watch from "./components/watch/Watch";
import OrgLogo from "./components/orglogo/OrgLogo";

import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect } from 'react';

//const socket = io('http://10.10.10.170:8080');

function App() {

  /*useEffect(()=>{
    socket.on('connect', ()=>{
      console.log("connected");
    });
    socket.on('disconnect', ()=>{
      console.log("disconnect");
    });

    socket.emit("wantTimetable", {})

  }, [])

  useEffect(()=>{
    socket.on("yourTimetable", (data)=>{
      console.log(data);
    })
  })*/
  return (
    <div className="App">

        <PhotoGallery />
        <OrgLogo />
        <Watch />
        <Timetable /*socket={socket}*//>

    </div>
  );
}

export default App;
